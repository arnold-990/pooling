﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StateMachine : MonoBehaviour
{
    [SerializeField] private State _currentState = null;

    public void Init(State state)
    {
        if (state != null)
        {
            _currentState = state;
            state.Sate();
        }
    }
    public void ChangeState(State state)
    {
        if (state != null)
        {
            _currentState.ExitFromState();

            _currentState = state;
            state.Sate();
        }
    }
}