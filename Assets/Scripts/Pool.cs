﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[System.Serializable]
public class Pool : MonoBehaviour
{
    public static Pool Instance = null;
    [System.Serializable]
    public class PoolObject
    {
        public GameObject prefab;
        public int amount;
    }
    public List<PoolObject> poolObjects;
    public List<GameObject> objectsOnScene;
    [SerializeField] [Header("Координаты родительского объекта")] Transform spawner;

    private void Start()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(this.gameObject);
        }
        foreach (PoolObject item in poolObjects)
        {
            for (int i = 0; i < item.amount; i++)
            {
                GameObject obj = Instantiate(item.prefab, spawner);
                objectsOnScene.Add(obj);
                obj.SetActive(false);
            }
        }
    }
    public GameObject Get(string tag)
    {
        foreach (GameObject item in objectsOnScene)
        {
            if (item.CompareTag(tag) && !item.activeInHierarchy)
            {
                item.SetActive(true);
                item.GetComponent<Rigidbody>().velocity = new Vector3(0, 15, 0);
                return item;
            }
        }
        return null;
    }
}