﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class EnemyController : MonoBehaviour
{
    [SerializeField] Transform[] goals;
    [SerializeField] private float speed = 3.5f;
    [SerializeField] private float accuracy = 0.5f;
    int currPoition;
    private void LateUpdate()
    {
        if (currPoition != goals.Length)
        {
            Vector3 lookAtTarget = new Vector3(goals[currPoition].position.x, this.transform.position.y, goals[currPoition].position.z);
            Debug.DrawLine(this.transform.position, goals[currPoition].transform.position, Color.red);
            this.transform.LookAt(lookAtTarget);
            if (Vector3.Distance(this.transform.position, lookAtTarget) > accuracy)
            {
                transform.Translate(0, 0, speed * Time.deltaTime);
            }
            else
            {
                currPoition++;
            }
        }
        else
        {
            currPoition = 0;
        }
    }
}